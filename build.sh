#!/bin/sh

export KERNELDIR=/home/michael/android/kernels/android_kernel_jewel
export PARENT_DIR=/home/michael/android/kernels
export INITRAMFS_DEST=$KERNELDIR/kernel/usr/initramfs
export INITRAMFS_SOURCE=$PARENT_DIR/Ramdisk/Jewel-JB
export PACKAGEDIR=$PARENT_DIR/Packages/Jewel
export PACKAGES=$PARENT_DIR/Packages
export DATE=$(date +"%A-%B-%d-%Y")
export TIME=$(date +"%r")
export ARCH=arm
export CROSS_COMPILE=/home/michael/android/arm-cortex-linaro-4.8/bin/arm-cortex_a8-linux-gnueabi-

echo ""
echo "*************** Today is $DATE-$TIME, M'kay ***************"
echo ""

echo "Make Package Directory"
rm -rf $PACKAGEDIR
mkdir $PACKAGEDIR

echo "Setup Package Directory"
mkdir -p $PACKAGEDIR/system/lib/modules
mkdir -p $PACKAGEDIR/system/etc/init.d

echo "Create initramfs dir"
mkdir -p $INITRAMFS_DEST

echo "Remove old initramfs dir"
rm -rf $INITRAMFS_DEST/*

echo "Copy new initramfs dir"
cp -R $INITRAMFS_SOURCE/* $INITRAMFS_DEST

echo "chmod initramfs dir"
chmod -R g-w $INITRAMFS_DEST/*
rm -rf $(find $INITRAMFS_DEST -name EMPTY_DIRECTORY -print)
rm -rf $(find $INITRAMFS_DEST -name .git -print)

echo "Remove old kcontrol_gpu_msm.ko"
rm $PACKAGES/kcontrol_gpu_msm.ko

echo "Remove old boot.img"
rm $PACKAGES/boot.img

echo "Remove old zImgae from Package"
rm $PACKAGES/kernel/zImage

echo "Make the kernel"
make clean
make mac_defconfig
make -j`grep 'processor' /proc/cpuinfo | wc -l`

if [ -e $KERNELDIR/arch/arm/boot/zImage ]; then
	echo "Make kcontrol gpu module"
	git clone https://github.com/showp1984/kcontrol_gpu_msm.git
	cd $KERNELDIR/kcontrol_gpu_msm
	sed -i '/KERNEL_BUILD := /c\KERNEL_BUILD := ../' Makefile
	make
	cd $KERNELDIR

	echo "Copy modules to Package"
	cp -a $(find . -name *.ko -print) $PACKAGEDIR/system/lib/modules/
	cp kcontrol_gpu_msm/kcontrol_gpu_msm.ko $PACKAGES

	echo "Remove temp kcontrol directory"
	rm -rf kcontrol_gpu_msm

	echo "Copy init.d scripts to Package"
	cp 01CIFS $PACKAGEDIR/system/etc/init.d/01CIFS
	cp 02TUN $PACKAGEDIR/system/etc/init.d/02TUN
	cp 04S2W $PACKAGEDIR/system/etc/init.d/04S2W
	cp 30ZRAM $PACKAGEDIR/system/etc/init.d/30ZRAM

	echo "Copy zImage to Package"
	cp arch/arm/boot/zImage $PACKAGEDIR/zImage
	cp arch/arm/boot/zImage $PACKAGES/kernel/zImage

	echo "Make boot.img"
	./mkbootfs $INITRAMFS_DEST | gzip > $PACKAGEDIR/ramdisk.gz
	./mkbootimg --kernel $PACKAGEDIR/zImage --ramdisk $PACKAGEDIR/ramdisk.gz --base 0x80400000 --pagesize 2048 --ramdiskaddr 0x82000000 --output $PACKAGES/boot.img

	echo "Make S-OFF kernel.zip"
	export curdate=`date "+%m-%d-%Y"`
	cd $PACKAGEDIR
	cp -R ../META-INF .
	cp -R ../kernel .
	cp -R ../prebuilt .
	rm ramdisk.gz
	rm zImage
	rm ../MAC_Sense*.zip
	zip -r ../MAC_Sense-$curdate.zip .
	cd $KERNELDIR
else
	echo "KERNEL DID NOT BUILD! no zImage exist"
fi;
